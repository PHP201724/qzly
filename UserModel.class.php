<?php
class UserModel extends Model {
    public function getUser()
    {
        $sql = 'select * from userinfo';
        return $this->dao->fetchAll($sql);
    }

    public function getUserById($user_id)
    {
        $sql = "select * from userinfo where user_id = $user_id";
        return $this->dao->fetchRow($sql);
    }

    public function addUser($userInput)
    {
        extract($userInput);
        $sql = "insert into userinfo values (null, '$user_name', md5($user_pass), '$user_gender', $user_tel, '$user_city', '$memo')";
        return $this->dao->my_query($sql);
    }

    public function userCount() {
        $sql = 'select count(*) from userinfo';
        return $this->dao->fetchColumn($sql);
    }

    public function delUserById($user_id)
    {
        $sql = "delete from userinfo where user_id = $user_id";
        return $this->dao->my_query($sql);
    }

    public function updateUserById($userInfo)
    {
        extract($userInfo);
        $sql = "update userinfo set user_name= '$user_name', user_tel='$user_tel', user_city='$user_city', memo='$memo' where user_id = $user_id";
        return $this->dao->my_query($sql);
    }

    public function realDelAllUser($user_id)
    {
        $sql = "delete from userinfo where user_id in ($user_id)";
        return $this->dao->my_query($sql);
    }
}