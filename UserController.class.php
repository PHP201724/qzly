<?php
class UserController extends Controller {
    public function indexAction()
    {

        $user = Factory::M('UserModel');
        $userInfo = $user->getuser();
        $this->assign('userInfo', $userInfo);
        $Count = $user->userCount();
        $this->assign('userCount', $Count['count(*)']);
        $this->display('info.html');
    }

    public function addAction()
    {
        $user = Factory::M('UserModel');
        $userInfo = $user->getuser();
        $this->assign('userInfo', $userInfo);
        $this->display('add.html');
    }

    public function dealaddAction()
    {
        $userInput = array();
        $userInput['user_name'] =$_POST['user_name'];
        $userInput['user_pass'] =$_POST['user_pass'];
        $userInput['user_gender'] =$_POST['user_gender'];
        $userInput['user_tel'] = (int)$_POST['user_tel'];
        $userInput['user_city'] =$_POST['user_city'];
        $userInput['memo'] =$_POST['memo'];

        if (empty($userInput['user_name'])|| empty($userInput['user_pass']) || empty($userInput['user_tel'])|| empty($userInput['memo']) || $_POST['user_pass'] != $_POST['con_pass']){
            $this->jump('index.php?p=Back&c=User&a=add', ':( 填写的信息不完整或两次密码不正确，请重新输入！');
        }
        $user = Factory::M('UserModel');
        $result = $user->addUser($userInput);
        if($result) {
            $this->jump('index.php?p=Back&c=User&a=index');
        } else {
            $this->jump('index.php?p=Back&c=User&a=add', ':( 发生未知错误，添加失败！');
        }
    }

    public function delAction() {
        $user_id = $_GET['user_id'];
        $user = Factory::M('UserModel');
        $result = $user->delUserById($user_id);
        if($result) {
            $this->jump('index.php?p=Back&c=User&a=index');
        } else {
            $this->jump("index.php?p=Back&c=User&a=index", ':( 发生奇怪错误，删除失败！');
        }
    }

    public function editAction() {
        $user_id = $_GET['user_id'];
        $user = Factory::M('UserModel');
        $userInfo = $user->getUserById($user_id);        
        $this->assign('userInfo', $userInfo);
        // var_dump($userInfo);die;
        $this->display('edit.html');
    }

    public function dealEditAction()
    {
        $userInfo = array();
        $userInfo['user_id'] = $_GET['user_id'];
        $userInfo['user_name'] =$_POST['user_name'];
        $userInfo['user_tel'] = (int)$_POST['user_tel'];
        $userInfo['user_city'] =$_POST['user_city'];
        $userInfo['memo'] =$_POST['memo'];

        if (empty($userInfo['user_name']) || empty($userInfo['user_tel'])|| empty($userInfo['memo'])){
            $this->jump('index.php?p=Back&c=User&a=edit', ':( 填写的信息不完整，请重新输入！');
        }
        $user = Factory::M('UserModel');
        $result = $user->updateUserById($userInfo);
        if($result) {
            $this->jump('index.php?p=Back&c=User&a=index');
        } else {
            $this->jump("index.php?p=Back&c=User&a=edit&user_id={$userInfo['user_id']}",':( 发生未知错误，修改失败！');
        }
    }

    public function delAllAction()
    {
        if(!isset($_POST['user_id'])) {
            $this->jump('index.php?p=Back&c=User&a=index', ':( 请先选择需要删除的文章');
        }
        $user_id = implode(',', $_POST['user_id']);
        $user = Factory::M('UserModel');
        $result = $user->realDelAllUser($user_id);
        if($result) {
            $this->jump('index.php?p=Back&c=User&a=index');
        } else {
            $this->jump("index.php?p=Back&c=User&a=index", ':( 发生未知错误，删除失败！');
        }
    }

    public function realDelAllAction()
    {
        if(!isset($_POST['user_id'])) {
            $this->jump('index.php?p=Back&c=User&a=index', ':( 请先选择需要删除的文章');
        }
        $user_id = implode(',', $_POST['user_id']);
        $user = Factory::M('UserModel');
        $result = $User->realDelAllUser($user_id);
        if($result) {
            $this->jump('index.php?p=Back&c=User&a=index');
        } else {
            $this->jump("index.php?p=Back&c=User&a=index", ':( 发生未知错误，删除失败！');
        }
    }
}